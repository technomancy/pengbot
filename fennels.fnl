(local {: view &as fennel} (require :fennel))
(local unpack (or table.unpack _G.unpack))

(local versions {})

(fn load-version [version]
  (os.execute (string.format "git -C checkout checkout %s" version))
  (os.execute "make -C checkout -B fennel.lua")
  (let [mod (dofile "checkout/fennel.lua")]
    (tset versions version #mod)
    mod))

(fn init []
  (os.execute "git clone -q https://git.sr.ht/~technomancy/fennel checkout")
  (set versions.main (partial load-version :main))
  (with-open [git (io.popen "git -C checkout tag")]
    (each [l (git:lines)]
      (tset versions l (partial load-version l)))))

(fn pull [?branch]
  (os.execute (string.format "git -C checkout checkout %s"
                             (or ?branch :main)))
  (os.execute "git -C checkout pull")
  (init))

(fn branch [branch-name]
  (assert (branch-name:match "^[-a-z]+$") "Illegal branch name!")
  (pull branch-name)
  (load-version branch-name))

(local init-env {: pairs : ipairs : tostring : tonumber : unpack : select
                 : assert : error : next : type : _VERSION : pcall : xpcall
                 : setmetatable : getmetatable :bit32 _G.bit32 : collectgarbage
                 : rawget : rawset : _G.rawlen : rawequal})

;; copy these in so they can't modify the originals
(each [_ mod (ipairs [:table :math :string :coroutine])]
  (tset init-env mod (collect [k v (pairs (. _G mod))] k v)))

(local unsafe-macros {:0.1.0 true :0.1.1 true :0.2.0 true :0.2.1 true
                      :0.3.0 true :0.3.1 true :0.3.2 true :0.4.0 true
                      :0.4.1 true :0.4.2 true :0.5.0 true :0.6.0 true
                      :master true})

(local envs {})

(fn preserve-macros [loader compiler original-macros]
  ;; this is a really nasty hack we have to do for older versions of fennel
  ;; because otherwise the "inner" macros of Fennel overshadow the "outer"
  ;; ones and we can't nest Fennel versions.
  (set compiler.scopes.compiler.macros {})
  (let [fennel (loader)]
    (set compiler.scopes.compiler.macros original-macros)
    fennel))

(fn require-fennel [version]
  (when (= nil (next versions))
    (init))
  (let [loader (assert (. versions version) (.. "Unknown version: " version))
        compiler-module :fennel.compiler
        compiler (require compiler-module) ; bypass include warning
        fennel (preserve-macros loader compiler compiler.scopes.compiler.macros)]
    (when (. unsafe-macros version)
      ;; it's impossible to sandbox these in older versions!
      (fennel.eval "(eval-compiler
                      (set _SPECIALS.eval-compiler nil)
                      (set _SPECIALS.macros nil))"))
    fennel))

(var printed nil)

(fn fakeprint [...]
  (table.insert (assert printed "can't print now") (table.concat [...] "\t")))

(fn reinit [version]
  (let [env (or (. envs version)
                (let [new-env {}]
                  (tset envs version new-env)
                  new-env))]
    (each [k (pairs env)]
      (tset env k nil))
    (each [k v (pairs init-env)]
      (tset env k v))
    (set env._G env)
    (set env.print fakeprint)
    (let [fennel (require-fennel version)]
      (set env.fennel {:version fennel.version
                       :doc fennel.doc
                       :gensym fennel.gensym
                       :mangle fennel.mangle
                       :unmangle fennel.unmangle
                       :path fennel.path
                       :view #(fennel.view $1 (or $2 {:one-line? true}))
                       :eval (fn [str options]
                               (let [opts []]
                                 (each [k v (pairs options)]
                                   (tset opts k v))
                                 (set opts.env env)
                                 (fennel.eval str opts)))
                       :traceback fennel.traceback}))
    env))

(fn view-each [options x ...]
  (if x
      (values (view x options) (view-each options ...))))

(fn multi-eval [version code]
  (set printed [])
  (let [env (or (. envs version) (reinit version))
        fennel (require-fennel version)]
    (match [(pcall fennel.eval code
                   {:env (assert env "environment not found; reinit")
                    ;; multi-line error messages aren't friendly in IRC
                    :unfriendly true
                    :useMetadata true})]
      [true ?val & vals] (values (table.concat
                                  [(view ?val {:one-line true}) ; in case of nil
                                   (view-each {:one-line true} (unpack vals))] " ")
                                 (-?> printed
                                      (table.concat "␊")
                                      (: :gsub "\n" " ")))
      [_ msg] msg)))

(fn multi-compile [version code]
  (let [fennel (require-fennel version)
        (ok val) (pcall fennel.compileString code {:indent "" :unfriendly true})
        oneline-val (if ok (string.gsub val "\n" " "))]
    (if ok oneline-val val)))

{: multi-eval : multi-compile : reinit : versions : pull : branch : init}
