# P'eng bot

> In the northern ocean there is a fish, called the k'un, I do not
> know how many thousand li in size. This k'un changes into a bird,
> called the p'eng. Its back is I do not know how many thousand li in
> breadth. When it is moved, it flies, its wings obscuring the sky
> like clouds.
>
> When on a voyage, this bird prepares to start for the Southern
> Ocean, the Celestial Lake. And in the Records of Marvels we read
> that when the p'eng flies southwards, the water is smitten for a
> space of three thousand li around, while the bird itself mounts upon
> a great wind to a height of ninety thousand li, for a flight of six
> months' duration.

- Chuang Tzu

An IRC bot written in [Fennel][fennel].

## Usage

    $ make pengbot
    $ ./pengbot irc.libera.chat 6667 my-pengbot "#pengbot" "#otherchan"

You can run it without ahead-of-time compilation, but this interferes
with loading multiple versions of Fennel.

Note that this does not use TLS to connect; it should not be used with
any sensitive data.

Commands are prefixed with a comma or can be addressed directly to the
bot's nick.

Run `,help` to get a list of commands. The main commands are:

* `,set TERM DEFINITION`: store a term and its definition
* `,get TERM`: retrieve a term's definition
* `,forget TERM`: delete a term's definition
* `,eval FORM`: evaluate a piece of Fennel code and print return value
* `,compile FORM`: print the output from compiling a piece of code
* `,multi-eval VERSION FORM`: run a piece of code in a specific Fennel version
* `,multi-compile VERSION FORM`: compile in a specific Fennel version
* `,versions`: list available versions of Fennel
* `,reinit VERSION`: reset environment for a specific Fennel version

Stores dictionary data (from `set` command) in the `data/` directory
that it's launched from. No limits are in place to prevent it from
filling up the disk yet, so disable that command if you're worried.

Requirements: Lua 5.1+ and [luasocket][luasocket].

## License

Copyright © 2018-2022 Phil Hagelberg and contributors

Distributed under the GNU General Public License version 3 or later; see file LICENSE.

[fennel]: https://fennel-lang.org
[luasocket]: http://w3.impa.br/~diego/software/luasocket/
