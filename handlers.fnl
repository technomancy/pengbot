(local {: view &as fennel} (require :fennel))
(local fennels (require :fennels))
(local unpack (or table.unpack _G.unpack))

(fn split [str ?pat] (icollect [s (str:gmatch (or ?pat "([%S]+)"))] s))

;; prevent directory traversal
(fn ok-term? [term] (and term (term:match "^[-%w]+$") term))

(fn get-term [term]
  (match (io.open (.. "data/" (assert (ok-term? term) "bad term")) "rb")
    f (let [data (f:read "*l")]
        (f:close)
        data)
    _ "not something I know about."))

(fn set-term [term data]
  (with-open [f (assert (io.open (.. "data/" (assert (ok-term? term) :bad!)) :w)
                        "Could not write term for some reason.")]
    (f:write data)
    (.. "OK, " term " is now " data)))

(fn fuzzy-get [filter]
  (match (icollect [l (: (io.popen "ls data") :lines)]
           (and (l:match filter) l))
    [a b &as terms] (.. "Found: " (table.concat terms ", "))
    [term] (.. term " is " (get-term term))
    _ (.. "No matches for " filter)))

;; ported from https://gist.github.com/mfiano/d7818a73eed89abaf0e32a9edf3f2b83
(fn jam-date [date]
  (let [formats {:day "%d" :month "%m" :dow "%a"}
        {: day : month : dow} (collect [k f (pairs formats)]
                                k (os.date f date))]
    (if (and (or (= "04" month) (= "10" month))
             (<= 15 (tonumber day))
             (= "Fri" dow))
        (os.date "%D" date)
        ;; one day
        (jam-date (+ date (* 60 60 24))))))

(fn jam []
  (.. "Next lisp jam is on " (jam-date (os.time))))

(fn tell [_ args]
  (match (split args)
    [nick :about term] (.. nick ": " term " is " (get-term term))
    _ "Usage: tell $NICK about $TERM"))

(fn help []
  (let [module-name :handlers] ; bypass include warning
    (.. "Commands are "
        (table.concat (icollect [k (pairs (require module-name))] k) ", ") ".")))

(local default-version :main)

(local snacks ["Yum; my favorite!"
               "Crunchy on the outside; chewy on the inside!"
               "I can't believe it's not butter!"
               "Thanks but I'm stuffed right now. Couldn't take another bite."
               "MMMMmmmmm..."])

(fn channel-name? [name] (name:match "^#[-_a-zA-Z0-9]+$"))

;; TODO: commands to join new channels?
{:botsnack #(. snacks (math.random (length snacks)))
 :source #"https://git.sr.ht/~technomancy/pengbot"
 :author #"technomancy (https://technomancy.us)"
 :ping #"pong!"
 :pingbot #"pongbot!"
 : help
 :echo #$2
 :set (fn [_ args] (set-term (args:match "^(%S+) (.*)")))
 :get (fn [_ term] (.. term " is " (get-term term)))
 :fuzzy-get (fn [_ filter]
              (fuzzy-get (assert (ok-term? filter) "bad filter")))
 ;; for when you want get, but without the prefix
 :whats (fn [_ term] (get-term term))
 :forget (fn [_ term]
           (assert (os.remove (.. "data/" (assert (ok-term? term) "bad!"))))
           "OK, forgot it.")
 :pull fennels.pull
 :jam jam
 :tell tell
 :branch (fn [_ args]
           (fennels.branch args)
           (string.format "Loaded Fennel from %s branch." args))
 :reinit (fn [_ args]
           (fennels.reinit (or args default-version))
           "Reinitialized eval environment.")
 :reload (fn [_ module-name]
           (let [mod (require module-name)]
             (tset package.loaded module-name nil)
             (each [k v (pairs (require module-name))]
               (tset mod k v))
             (tset package.loaded module-name mod))
           "OK, reloaded")
 :debug (fn []
          (set _G.debug? (not _G.debug?))
          (if _G.debug? "Debug enabled." "Debug disabled."))
 :multi-compile (fn [_ args]
                  (let [(version code) (args:match "^(%S+) (.*)")]
                    (fennels.multi-compile version code)))
 :compile (fn [_ code] (fennels.multi-compile default-version code))
 :eval (fn [_ code] (fennels.multi-eval default-version code))
 :multi-eval (fn [_ args]
               (let [(version code) (args:match "^(%S+) (.*)")]
                 (if (. fennels.versions version)
                     (fennels.multi-eval version code)
                     (.. "Fennel version " version " not found."))))
 :versions (fn []
             (fennels.init)
             (let [versions (icollect [k (pairs fennels.versions)] k)]
               (.. "Available Fennel versions: " (table.concat versions " "))))
 :join (fn [_ channel-name conn]
         (when (channel-name? channel-name)
           (let [{: join} (require :irc)]
             (join conn channel-name))))
 :part (fn [_ channel-name conn]
         (when (channel-name? channel-name)
           (let [{: part} (require :irc)]
             (part conn channel-name))))}
