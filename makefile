SRC=pengbot.fnl irc.fnl handlers.fnl fennels.fnl
LUA_VERSION=5.4.2

pengbot: $(SRC) socket.a mime.a lua-$(LUA_VERSION)/src/liblua.a
	./fennel --globals "" --compile-binary $< $@ \
		lua-$(LUA_VERSION)/src/liblua.a lua-$(LUA_VERSION)/src \
		--native-module socket.a --native-module mime.a

lua-$(LUA_VERSION): ; curl http://www.lua.org/ftp/lua-$(LUA_VERSION).tar.gz | tar xzf /dev/stdin
lua-$(LUA_VERSION)/src/liblua.a: lua-$(LUA_VERSION)
	make -C $^

count: ; cloc $(SRC)

luasocket: ; git clone https://github.com/diegonehab/luasocket

SOCKET_OBJS= \
	luasocket/src/luasocket.o \
	luasocket/src/timeout.o \
	luasocket/src/buffer.o \
	luasocket/src/io.o \
	luasocket/src/auxiliar.o \
	luasocket/src/compat.o \
	luasocket/src/options.o \
	luasocket/src/inet.o \
	luasocket/src/except.o \
	luasocket/src/select.o \
	luasocket/src/tcp.o \
	luasocket/src/udp.o \
	luasocket/src/usocket.o # <- this one should be wsocket.o for Windows

luasocket/src/%.o: luasocket lua-$(LUA_VERSION)
	make -C luasocket/src linux install MYCFLAGS=-static \
		LUAV=5.4 DESTDIR=$(PWD) LUAINC_linux=$(PWD)/lua-$(LUA_VERSION)/src

socket.a: $(SOCKET_OBJS)
	ar rcs $@ $^

mime.a: luasocket/src/mime.o luasocket/src/compat.o
	ar rcs $@ $^

run: ; ./fennel pengbot.fnl irc.libera.chat 6667 pengbot-dev "#pengbot"

update-fennel: ../fennel/fennel ../fennel/fennel.lua
	cp $^ .

clean:
	rm -fr pengbot
	make -C luasocket clean
	make -C lua-$(LUA_VERSION) clean

.PHONY: run update-fennel clean
