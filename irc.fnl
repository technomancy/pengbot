(local handlers (require :handlers))

(fn send [socket command use-separator? args]
  (var str command)
  (when (> (# args) 0)
    (let [sep (if use-separator?
                  " :"
                  (> (# args) 1)
                  " " "")]
      (set str (.. str " " (table.concat args " " 1 (- (# args) 1))
                   sep (. args (# args))))))
  (when _G.debug? (print ">" str))
  (socket:send (.. str "\r\n")))

(fn push [conn command use-separator? args]
  (tset conn.queue conn.bottom [command use-separator? args])
  (set conn.bottom (+ conn.bottom 1)))

(fn tokenize [line]
  (let [(_ e1 prefix)  (line:find "^:(%S+)")
        (_ e2 command) (line:find "(%S+)" (if e1 (+ e1 1) 1))
        (_ _ rest)     (line:find "%s+(.*)" (if e2 (+ e2 1) 1))]
    (values prefix command rest)))

(fn flush [conn]
  (when (not= conn.top conn.bottom)
    (let [[command separator? args] (. conn.queue conn.top)]
      (tset conn.queue conn.top nil)
      (set conn.top (+ conn.top 1))
      (when (= conn.top conn.bottom)
        (set conn.top 1)
        (set conn.bottom 1))
      (send conn.socket command separator? args))))

(local irc {:commands {} : tokenize : flush})

;; define functions for outgoing IRC operations
(each [name separator? (pairs {:nick false :user true :join false :privmsg true
                               :ping false :pong false :part true :kick true})]
  (tset irc name
        (fn [conn ...]
          (push conn (name:upper) separator? [...]))))

;; commands are like protocol-level handlers, whereas the handlers
;; module is for application-level handlers.
(fn irc.commands.ping [conn prefix rest] (irc.pong conn rest))

(fn irc.commands.privmsg [conn prefix rest]
  (let [chan (rest:match "(%S+)")
        msg (rest:match ":(.*)")
        their-nick (or (prefix:match "(%S+)!") prefix)
        my-nick (conn.nick:gsub "-" "%%-")
        chan (if (chan:find "^#") chan their-nick)
        (cmd args) (if (msg:find "^,")
                       (msg:match "^,(%S+) ?(.*)")
                       (msg:match (string.format "^%s: (%%S+) ?(.*)" my-nick)))
        handler (. handlers cmd)]
    (when (and handler _G.debug?)
      (print "H>" cmd args))
    (when handler
      (match (pcall handler chan args conn)
        (true resp ?out) (when (= :string (type resp))
                           (when (= :string (type ?out))
                             (irc.privmsg conn chan ?out))
                           ;; if a form printed something and returned nil
                           ;; don't print the nil
                           (when (not (and (= :string (type ?out))
                                           (= :nil resp)))
                             (irc.privmsg conn chan resp)))
        (false err) (do (print "Error in handler" err)
                        (irc.privmsg conn chan (.. "Error: " err)))))))

;; after you've identified and are properly connected
(fn join-handler [conn prefix rest]
  (print "Connected!")
  (each [_ channel (ipairs conn.channels)]
    (let [channel (if (channel:find "^#")
                      channel (.. "#" channel))]
      (irc.join conn channel))))

(tset irc.commands "376" join-handler)

irc
